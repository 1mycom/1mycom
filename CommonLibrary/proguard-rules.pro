# This is a configuration file for ProGuard.
# http://proguard.sourceforge.net/index.html#manual/usage.html

-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-verbose

# Optimization is turned off by default. Dex does not like code run
# through the ProGuard optimize and preverify steps (and performs some
# of these optimizations on its own).
-dontoptimize
-dontpreverify
# Note that if you want to enable optimization, you cannot just
# include optimization flags in your own project configuration file;
# instead you will need to point to the
# "proguard-android-optimize.txt" file instead of this one from your
# project.properties file.

-keep public class com.google.vending.licensing.ILicensingService
-keep public class com.android.vending.licensing.ILicensingService

-keep class android.support.v4.app.** { *; }
-keep interface android.support.v4.app.** { *; }

# For native methods, see http://proguard.sourceforge.net/manual/examples.html#native
-keepclasseswithmembernames class * {
    native <methods>;
}

-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.preference.Preference

# keep setters in Views so that animations can still work.
# see http://proguard.sourceforge.net/manual/examples.html#beans
-keepclassmembers public class * extends android.view.View {
   void set*(***);
   *** get*();
}

# We want to keep methods in Activity that could be used in the XML attribute onClick
-keepclassmembers class * extends android.app.Activity {
   public void *(android.view.View);
}

# For enumeration classes, see http://proguard.sourceforge.net/manual/examples.html#enumerations
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keepclassmembers class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator CREATOR;
}

-keepclassmembers class **.R$* {
    public static <fields>;
}

-keep public class * extends android.view.View {
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
    public void set*(...);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

#Butterknife library
#-keep class butterknife.** { *; }
#-keep class **$$ViewBinder { *; }
#-keepclasseswithmembernames class * {
#    @butterknife.* <fields>;
#}
#-keepclasseswithmembernames class * {
#    @butterknife.* <methods>;
#}
#-dontwarn butterknife.**
#-dontnote butterknife.**

# OkHttp library
-keepattributes Signature
-keepattributes *Annotation*
-keep class com.squareup.okhttp.** { *; }
-keep interface com.squareup.okhttp.** { *; }


# Picaso libarary
-dontwarn com.squareup.okhttp.**
-dontnote com.squareup.okhttp.**


## Nineolddroid related classes to ignore

#-keep class com.nineoldandroids.animation.** { *; }
#-keep interface com.nineoldandroids.animation.** { *; }
#-keep class com.nineoldandroids.view.** { *; }
#-keep interface com.nineoldandroids.view.** { *; }



#-libraryjars libs/ViewHelperLib.jar
#-libraryjars libs/httpmime-4.3.5.jar
#-libraryjars libs/httpcore-4.3.2.jar
#-libraryjars libs/httpclient-4.3.5.jar
#-libraryjars libs/mail.jar
#-libraryjars libs/additionnal.jar
#-libraryjars libs/activation.jar

-keep class org.apache.http.** { *; }
-keep class com.google.common.** {*;}
-keep class com.google.android.gms.** {*;}
-keep class com.google.android.gms.** {*;}

-keep class javax.** {*;}
-keep class com.sun.** {*;}
-keep class myjava.** {*;}

-keep public class Mail {*;}

-dontwarn java.beans.Beans
-dontwarn javax.security.**



# The support library contains references to newer platform versions.
# Don't warn about those in case this app is linking against an older
# platform version.  We know about them, and they are safe.
-dontwarn android.support.**
-dontwarn org.apache.http.**
-dontwarn com.google.common.**
-dontwarn com.google.android.gms.**
-dontwarn java.awt.**
-dontwarn okio.**
-dontwarn org.apache.harmony.**
-dontwarn myjava.**


#Specifies not to print notes about potential mistakes or omissions in the configuration,
#such as typos in class names or missing options that might be useful.
#The optional filter is a regular expression; ProGuard doesn't print notes about classes with matching names.

-dontnote android.support.**
-dontnote org.apache.http.**
-dontnote com.google.common.**
-dontnote com.google.android.gms.**
-dontnote java.awt.**
-dontnote okio.**
-dontnote org.apache.harmony.**
-dontnote myjava.**