package com.bv.commonlibrary.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

import com.bv.commonlibrary.R;
import com.bv.commonlibrary.constants.FontConstants;
import com.bv.commonlibrary.util.TypefaceManager;


/**
 * Custom Button to set different fonts.
 * 
 * "fontName" is the key attribute to set fonts from XML Layouts
 * 
 * @author suryaprakash.konduru
 * 
 */
public class FancyAppCompactButton extends AppCompatButton {

	public FancyAppCompactButton(Context _Context) {

		super(_Context);
		init(null);
		isInEditMode();
	}

	public FancyAppCompactButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		init(attrs);
		isInEditMode();
	}

	/**
	 * Reads font name attribute from attribute and sets font to TextView
	 * 
	 * @param attrs
	 */
	private void init(AttributeSet attrs) {

		if (!isInEditMode() && attrs != null) {
			TypedArray a = getContext().obtainStyledAttributes(attrs,
					R.styleable.Fonts);
			Integer fontName = a.getInteger(R.styleable.Fonts_fontName, 0);
			if (fontName != null) {
				TypefaceManager.setTypeface(this, FontConstants
						.getFontNameFromFontValue(fontName));
			}
			a.recycle();
		}
	}

}
