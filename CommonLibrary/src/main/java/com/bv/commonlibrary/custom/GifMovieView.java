package com.bv.commonlibrary.custom;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Movie;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;

import com.bv.commonlibrary.util.Log;

import java.io.IOException;
import java.io.InputStream;

public class GifMovieView extends View {

	private Movie mMovie;
	private long mMoviestart;
	InputStream stream;


	public GifMovieView(Context context, AttributeSet attrs) {
		super(context, attrs);
		try {
			DisplayMetrics dm = getResources().getDisplayMetrics();
			Log.print("Dpi:"+dm.densityDpi);
			if(dm.densityDpi > 320)
				stream = context.getAssets().open("loader.gif");
			else
				stream = getContext().getAssets().open("loader_small.gif");
		} catch (IOException e) {
			e.printStackTrace();
		}
		mMovie = Movie.decodeStream(stream);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		canvas.drawColor(Color.TRANSPARENT);
		super.onDraw(canvas);
		final long now = SystemClock.uptimeMillis();

		if (mMoviestart == 0) {
			mMoviestart = now;
		}

		final int relTime = (int) ((now - mMoviestart) % mMovie.duration());
		mMovie.setTime(relTime);

//		Log.debug("System out", "width:"+getWidth()+"::::mMovie.width():"+mMovie.width());
//		Log.debug("System out", "height:"+getHeight()+":::mMovie.height():"+mMovie.height());

		mMovie.draw(canvas, (getWidth() / 2) - (mMovie.width() / 2),
				(getHeight() / 2) - (mMovie.height() / 2));

//		mMovie.draw(canvas, 60 - (mMovie.width() / 2),
//				60 - (mMovie.height() / 2));
//		mMovie.draw(canvas, 0,0);
		this.invalidate();
	}
}