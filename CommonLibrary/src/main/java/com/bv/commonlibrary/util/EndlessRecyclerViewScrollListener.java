package com.bv.commonlibrary.util;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.widget.AbsListView;
/**
 * Created by Administrator on 4/29/2016.
 */
public abstract class EndlessRecyclerViewScrollListener extends RecyclerView.OnScrollListener{
    // The minimum amount of items to have below your current scroll position
    // before loading more.
    /*private int visibleItemCount = 1;
    // The current offset index of data you have loaded
    private int currentPage = 0;
    // The total number of items in the dataset after the last load
    private int previousTotalItemCount = 0;
    // True if we are still waiting for the last set of data to load.
    private boolean loading = true;
    // Sets the starting page index
    private int startingPageIndex = 0;*/

    /**
     * Variables for load more data on list scroll down
     */
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private int mPage = 0;
    private int mPageLimit = 10;

    RecyclerView.LayoutManager mLayoutManager;

    public EndlessRecyclerViewScrollListener(LinearLayoutManager layoutManager, int mPageLimit) {
        this.mLayoutManager = layoutManager;
        this.mPageLimit=mPageLimit;
    }

    public EndlessRecyclerViewScrollListener(GridLayoutManager layoutManager, int mPageLimit) {
        this.mLayoutManager = layoutManager;
        visibleItemCount = visibleItemCount * layoutManager.getSpanCount();
        this.mPageLimit=mPageLimit;
    }

    public EndlessRecyclerViewScrollListener(StaggeredGridLayoutManager layoutManager, int mPageLimit) {
        this.mLayoutManager = layoutManager;
        visibleItemCount = visibleItemCount * layoutManager.getSpanCount();
        this.mPageLimit=mPageLimit;
    }


    // This happens many times a second during a scroll, so be wary of the code you place here.
    // We are given a few useful parameters to help us work out if we need to load some more data,
    // but first we check if we are waiting for the previous load to finish.
    @Override
    public void onScrolled(RecyclerView view, int dx, int dy) {

    }

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
        if (newState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {

            visibleItemCount = mLayoutManager.getChildCount();
            totalItemCount = mLayoutManager.getItemCount();
            if (mLayoutManager instanceof StaggeredGridLayoutManager){
                int [] firstVisibleItems = ((StaggeredGridLayoutManager)mLayoutManager).findFirstVisibleItemPositions(null);
                if(firstVisibleItems != null && firstVisibleItems.length > 0) {
                    pastVisiblesItems = firstVisibleItems[0];
                }
            }else if (mLayoutManager instanceof LinearLayoutManager) {
                pastVisiblesItems = ((LinearLayoutManager) mLayoutManager).findFirstVisibleItemPosition();
            } else if (mLayoutManager instanceof GridLayoutManager) {
                pastVisiblesItems = ((GridLayoutManager) mLayoutManager).findFirstVisibleItemPosition();
            }

            if (loading && totalItemCount>=mPageLimit)
            {
                if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                {
                    android.util.Log.e("System out", "\nvisibleItemCount::: "+visibleItemCount+"\npastVisiblesItems::: "+pastVisiblesItems
                            +"\ntotalItemCount::: "+totalItemCount);
                    loading = false;
                    Log.print("...", "Last Item Wow !");
                    //Do pagination.. i.e. fetch new data
                    mPage++;
                    onLoadMore(mPage, pastVisiblesItems);
                }
            }

        }
    }

    // Defines the process for actually loading more data based on page
    public abstract void onLoadMore(int page, int previousTotalItemCount);
}