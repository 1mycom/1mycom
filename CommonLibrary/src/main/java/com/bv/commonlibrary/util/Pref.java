package com.bv.commonlibrary.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Base64;
import android.util.Base64InputStream;
import android.util.Base64OutputStream;

import com.bv.commonlibrary.constants.CommonConstants;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.util.HashSet;
import java.util.Set;


@SuppressLint("NewApi")
public class Pref {

	private static SharedPreferences sharedPreferences = null;

	public static void openPref(Context context) {

		sharedPreferences = context.getSharedPreferences(CommonConstants.PREF_FILE,
				Context.MODE_PRIVATE);

	}

	public static String getValue(Context context, String key,
			String defaultValue) {
		Pref.openPref(context);
		String result = Pref.sharedPreferences.getString(key, defaultValue);
		Pref.sharedPreferences = null;
		return result;
	}

	public static void setValue(Context context, String key, String value) {
		Pref.openPref(context);
		Editor prefsPrivateEditor = Pref.sharedPreferences.edit();
		prefsPrivateEditor.putString(key, value);
		prefsPrivateEditor.commit();
		prefsPrivateEditor = null;
		Pref.sharedPreferences = null;
	}

	public static boolean getValue(Context context, String key,
			boolean defaultValue) {
		Pref.openPref(context);
		boolean result = Pref.sharedPreferences.getBoolean(key, defaultValue);
		Pref.sharedPreferences = null;
		return result;
	}

	public static void setValue(Context context, String key, boolean value) {
		Pref.openPref(context);
		Editor prefsPrivateEditor = Pref.sharedPreferences.edit();
		prefsPrivateEditor.putBoolean(key, value);
		prefsPrivateEditor.commit();
		prefsPrivateEditor = null;
		Pref.sharedPreferences = null;
	}
	public static int getValue(Context context, String key,
			int defaultValue) {
		Pref.openPref(context);
		int result = Pref.sharedPreferences.getInt(key, defaultValue);
		Pref.sharedPreferences = null;
		return result;
	}

	public static void setValue(Context context, String key, int value) {
		Pref.openPref(context);
		Editor prefsPrivateEditor = Pref.sharedPreferences.edit();
		prefsPrivateEditor.putInt(key, value);
		prefsPrivateEditor.commit();
		prefsPrivateEditor = null;
		Pref.sharedPreferences = null;
	}
	public static void setValue(Context context, String key, long value) {
		Pref.openPref(context);
		Editor prefsPrivateEditor = Pref.sharedPreferences.edit();
		prefsPrivateEditor.putLong(key, value);
		prefsPrivateEditor.commit();
		prefsPrivateEditor = null;
		Pref.sharedPreferences = null;
	}
	public static void setStringSet(Context _Context, String key,
			Set<String> mSetArray) {

		Pref.openPref(_Context);

		Editor preferenceEditor = Pref.sharedPreferences.edit();
		preferenceEditor.putStringSet(key, mSetArray);
		preferenceEditor.commit();
		preferenceEditor = null;
		Pref.sharedPreferences = null;
	}

	public static Set<String> getStoredPassHistory(Context _Context, String mKey) {

		Pref.openPref(_Context);

		HashSet<String> mSetPassHistory = (HashSet<String>) Pref.sharedPreferences
				.getStringSet(mKey, new HashSet<String>());

		Pref.sharedPreferences = null;

		return mSetPassHistory;
	}
	public static Editor getPrefEditor(Context mContext){
		Pref.openPref(mContext);
		
		Editor mEditor = Pref.sharedPreferences.edit();
		return mEditor;
	}

	public static void setBeanValue(Context context, String key, Object object) {
		Pref.openPref(context);
		Editor prefsPrivateEditor = Pref.sharedPreferences.edit();

		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();

		ObjectOutputStream objectOutput;
		try {
			objectOutput = new ObjectOutputStream(arrayOutputStream);
			objectOutput.writeObject(object);
			byte[] data = arrayOutputStream.toByteArray();
			objectOutput.close();
			arrayOutputStream.close();

			ByteArrayOutputStream out = new ByteArrayOutputStream();
			Base64OutputStream b64 = new Base64OutputStream(out, Base64.DEFAULT);
			b64.write(data);
			b64.close();
			out.close();

			prefsPrivateEditor.putString(key, new String(out.toByteArray()));

			prefsPrivateEditor.commit();
		} catch (IOException e) {
			e.printStackTrace();
		}

		prefsPrivateEditor = null;
		Pref.sharedPreferences = null;
	}

	public static Object getBeanValue(Context context, String key) {
		Pref.openPref(context);

		byte[] bytes = Pref.sharedPreferences.getString(key, "{}").getBytes();
		if (bytes.length == 0) {
			return null;
		}
		ByteArrayInputStream byteArray = new ByteArrayInputStream(bytes);
		Base64InputStream base64InputStream = new Base64InputStream(byteArray, Base64.DEFAULT);
		ObjectInputStream in = null;
		Object myObject = new Object();
		try {
			in = new ObjectInputStream(base64InputStream);
			try {
				myObject = in.readObject();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (StreamCorruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (in!=null) {
			try {
				in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Pref.sharedPreferences = null;
		return myObject;
	}

	public static void clearData(Context context, String key){
		Pref.openPref(context);
		Editor prefsPrivateEditor = Pref.sharedPreferences.edit();
		prefsPrivateEditor.remove(key);
		prefsPrivateEditor.commit();
	}
	public static void clearAllData(Context context){
		Pref.openPref(context);
		Editor prefsPrivateEditor = Pref.sharedPreferences.edit();
		prefsPrivateEditor.clear();
		prefsPrivateEditor.commit();
	}


}
