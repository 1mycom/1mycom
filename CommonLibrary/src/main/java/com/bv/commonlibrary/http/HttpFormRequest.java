package com.bv.commonlibrary.http;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.bv.commonlibrary.R;
import com.bv.commonlibrary.util.Log;
import com.bv.commonlibrary.util.Utils;

import org.apache.http.NameValuePair;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
//import okhttp3.OkHttpClient;

/**
 * Created by Keyur on 4/1/16.
 */
public class HttpFormRequest extends AsyncTask<Void, Void, String> {
    Context mContext;

//    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    private OkHttpClient client = new OkHttpClient();

    AsyncTaskCompleteListener mAsyncTaskCompleteListener;
    Dialog dialog;

    private String urlString = "";
    private RequestBody mRequestBody = null;
    private boolean mIsShow;
    private boolean isMultipartReq = false;
    private File file = null;

    InputStream inputStream = null;
    int statusCode;

    String twoHyphens = "--";
    String boundary = "*****" + Long.toString(System.currentTimeMillis()) + "*****";
    String lineEnd = "\r\n";


    int bytesRead, bytesAvailable, bufferSize;
    byte[] buffer;
    int maxBufferSize = 1 * 1024 * 1024;

    HttpURLConnection connection = null;
    DataOutputStream outputStream = null;
    private List<NameValuePair> objValuePair = null;
    String imgParam;
    String token = "";


   private static final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");

    public HttpFormRequest(Context _context) {
        mContext = _context;
    }


    public HttpFormRequest(Context _context, String url, RequestBody req, boolean mIsShow, AsyncTaskCompleteListener _asyncTaskCompleteListener) {
        mContext = _context;
        mAsyncTaskCompleteListener = _asyncTaskCompleteListener;
        urlString = url;
        mRequestBody = req;
        this.mIsShow = mIsShow;
        this.isMultipartReq = false;
        this.file = null;
    }

    /**
     * @param url:                       API URL
     * @param _objvaluepair              : Request parameter in List<NameValuePair> format
     * @param mIsShow                    : boolean to show progressbar
     * @param _isMultipartReq            : flag to identify mRequestBody Type( normal or multipart)
     * @param _file                      : file object
     * @param _imgParam                  : Api parameter name(key) for image upload
     * @param _asyncTaskCompleteListener : interface object to get response.
     */

    public HttpFormRequest(Context _context, String url, String token, List<NameValuePair> _objvaluepair, boolean mIsShow, boolean _isMultipartReq, File _file,
                           String _imgParam, AsyncTaskCompleteListener _asyncTaskCompleteListener) {
        mContext = _context;
        mAsyncTaskCompleteListener = _asyncTaskCompleteListener;
        urlString = url;
        mRequestBody = null;
        objValuePair = _objvaluepair;
        this.mIsShow = mIsShow;
        this.isMultipartReq = _isMultipartReq;
        this.file = _file;
        this.imgParam = _imgParam;
        this.token = token;
    }


    public HttpFormRequest(Context _context, String url, String token, RequestBody req, boolean mIsShow, boolean _isMultipartReq, File _file,
                           String _imgParam, AsyncTaskCompleteListener _asyncTaskCompleteListener) {
        mContext = _context;
        mAsyncTaskCompleteListener = _asyncTaskCompleteListener;
        urlString = url;
        mRequestBody = req;
        this.mIsShow = mIsShow;
        this.isMultipartReq = _isMultipartReq;
        this.file = _file;
        this.imgParam = _imgParam;
        this.token = token;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (mIsShow) {
            showProgressBar();
        }
    }

    public HttpFormRequest(Context _context, String url, String token, RequestBody req, boolean mIsShow, AsyncTaskCompleteListener _asyncTaskCompleteListener) {
        mContext = _context;
        mAsyncTaskCompleteListener = _asyncTaskCompleteListener;
        urlString = url;
        mRequestBody = req;
        this.token = token;
        this.mIsShow = mIsShow;
        this.isMultipartReq = false;
        this.file = null;
    }

    @Override
    protected String doInBackground(Void... params) {
        if (!isMultipartReq) {
            if (mRequestBody == null)
                return getRequestBody();
            else
                return postRequest();
        } else {
            return submitPost(mContext, mRequestBody, urlString, file, imgParam);
            //return uploadMultipartData(mContext, mRequestBody, urlString, file, imgParam);
        }
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (mIsShow) {
            hideProgressBar();
        }
        Log.print("Response:" + result);
        mAsyncTaskCompleteListener.asyncTaskComplted(result);
    }


    public String uploadMultipartData(Context mContext, String req, String urlString, File file, String imgParam) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);
        try {

            Log.print("URL:" + urlString);
            Log.print("Request:" + mRequestBody);

            client = new OkHttpClient.Builder()
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .build();

//            RequestBody body = RequestBody.create(JSON, mRequestBody);
            MultipartBody.Builder multipartBuilder = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
//                    .addHeader("Content-Type","application/json")
//                    .addPart(body)
//                    .addPart(Headers.of("Content-Type", "application/json", "authorization", "Bearer "+token), body )
                    .addPart(Headers.of("Content-Disposition", "form-data; name=name=\"" + imgParam + ""),
                            RequestBody.create(MEDIA_TYPE_PNG, file));


            RequestBody requestBody = multipartBuilder
                    .build();


            Request request = new Request.Builder()
                    .url(urlString)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("authorization", "Bearer " + token)
                    .post(requestBody)
                    .build();

            Response response = client.newCall(request).execute();
            return response.body().string();


//            MultipartBody.Builder buildernew = new MultipartBody.Builder()
//                    .setType(MultipartBody.FORM)
//                    .addFormDataPart("title", title);   //Here you can add the fix number of data.
//
//            for (int i = 0; i < AppConstants.arrImages.size(); i++) {
//                File f = new File(FILE_PATH,TEMP_FILE_NAME + i + ".png");
//                if (f.exists()) {
//                    buildernew.addFormDataPart(TEMP_FILE_NAME + i, TEMP_FILE_NAME + i + FILE_EXTENSION, RequestBody.create(MEDIA_TYPE, f));
//                }
//            }
//
//            MultipartBody requestBody = buildernew.build();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     * @param mContext  : Context
     * @param req       : mRequestBody paramters
     * @param urlString : API URL
     * @param file      : File object
     * @param imgParam: Api parameter key to upload image
     */
    public String submitPost(Context mContext, RequestBody req, String urlString, File file, String imgParam) {
        try {
            Log.print("URL:" + urlString);
            Log.print("Request:" + mRequestBody);

            FileInputStream fileInputStream = null;
            HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
            String[] q = new String[0];
            int idx = 0;
            if (file != null) {
                q = file.getAbsoluteFile().toString().split("/");
                idx = q.length - 1;
                fileInputStream = new FileInputStream(file);
            }


            URL url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();

            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);

            connection.setRequestMethod("POST");
            connection.setRequestProperty("Connection", "Keep-Alive");
            //connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("authorization", "Bearer " + token);
            connection.setRequestProperty("User-Agent", "Android Multipart HTTP Client 1.0");
            connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);


            if (file != null)
                connection.setRequestProperty("\"" + imgParam + "\"", q[idx]);
            else
                connection.setRequestProperty("\"" + imgParam + "\"", "");

            outputStream = new DataOutputStream(connection.getOutputStream());
            outputStream.writeBytes(twoHyphens + boundary + lineEnd);

            //------- check whether file is blank or not
            //------- if blank pass blank value
            //------- else pass file object
            if (file != null) {
                outputStream.writeBytes("Content-Disposition: form-data;  name=\"" + imgParam + "\";filename=\"" + q[idx] + "\"" + lineEnd);
            } else {
                String blank = "";
                outputStream.writeBytes("Content-Disposition: form-data;  name=\"" + imgParam + "\";filename=\"" + blank + "\"" + lineEnd);
            }
            outputStream.writeBytes("Content-Type: " + "image/jpg" + lineEnd);
            outputStream.writeBytes("Content-Transfer-Encoding: binary" + lineEnd);


            outputStream.writeBytes(lineEnd);

            if (file != null) {
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];

                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                while (bytesRead > 0) {
                    outputStream.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }

                outputStream.writeBytes(lineEnd);
            }

            // Upload POST Data
            for (int i = 0; i < objValuePair.size(); i++) {
                NameValuePair objPair = objValuePair.get(i);
                outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                outputStream.writeBytes("Content-Disposition: form-data; name=\"" + objPair.getName() + "\"" + lineEnd);
                outputStream.writeBytes("Content-Type: text/plain" + lineEnd);
                outputStream.writeBytes(lineEnd);
                outputStream.writeBytes(objPair.getValue());
                outputStream.writeBytes(lineEnd);
            }
            outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            statusCode = connection.getResponseCode();
            String serverResponseMessage = connection.getResponseMessage();

            InputStream is = new BufferedInputStream(connection.getInputStream());
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuilder sb = new StringBuilder();
            while ((line = rd.readLine()) != null) {
                sb.append(line);
            }

            Log.debug("Is", "IS:" + sb.toString());
            Log.debug("uploadFile", "HTTP Response is : " + serverResponseMessage + ": " + statusCode);

            if (file != null)
                fileInputStream.close();
            outputStream.flush();
            outputStream.close();

            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }




    /***
     * Post Request
     */
    public String postRequest() {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);
        try {

            Log.print("URL:" + urlString);
            Log.print("Request:" + mRequestBody);

            client = new OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
//                    .connectionSpecs(Collections.singletonList(spec))
                    .build();

            /*if API needs to authenticate than add credential in header and replace username/password with your own credentials*/
            Request request = null;
            if (Utils.isNotNull(token)) {
                request = new Request.Builder()
//                        .addHeader("Content-Type", "application/json")
                        .addHeader("Mobiconnectheader", "mobiconnectheaderapikey")
//                        .post(mRequestBody)
                        .url(urlString)
                        .post(mRequestBody)
                        .build();
            } else {
                request = new Request.Builder()
                        .addHeader("Mobiconnectheader", "mobiconnectheaderapikey")
                        .url(urlString)
                        .post(mRequestBody)
                        .build();
            }


            Response response = client.newCall(request).execute();
            return response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
            hideProgressBar();
            return null;
        }
    }

    /**
     * Get Request with/without token
     */
    public String getRequestBody() {
        Log.print("URL:" + urlString);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try {
            /*if API needs to authenticate than add Headers*/
            Request request = null;
            if (Utils.isNotNull(token)) {
                request = new Request.Builder()
                        .addHeader("Mobiconnectheader", "mobiconnectheaderapikey")
                        .url(urlString)
                        .build();
            } else {
                request = new Request.Builder()
                        .addHeader("Mobiconnectheader", "mobiconnectheaderapikey")
                        .url(urlString)
                        .build();
            }
            Response response = client.newCall(request).execute();
            return response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     * Show Dialog During web service calling
     */
    public void showProgressBar() {
        dialog = new Dialog(mContext, android.R.style.Theme_Translucent);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        dialog.setCancelable(false);

        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewChild = inflater.inflate(R.layout.loader, null);

        dialog.setContentView(viewChild);

        Runtime.getRuntime().gc();
        System.gc();

        try {
            dialog.show();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }


    /**
     * Hide progress dialog
     */
    public void hideProgressBar() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    private class NullHostNameVerifier implements HostnameVerifier {

        @Override
        public boolean verify(String hostname, SSLSession session) {
            Log.debug("RestUtilImpl", "Approving certificate for " + hostname);
            return true;
        }
    }

    public interface AsyncTaskCompleteListener {
        public void asyncTaskComplted(String response);

    }

}