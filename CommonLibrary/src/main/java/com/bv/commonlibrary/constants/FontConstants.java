package com.bv.commonlibrary.constants;


/** 
 * This enum is tightly coupled with the enum in res/values/attrs.xml! 
 * Make sure their orders stay the same **
 * fontValue - should be mapped with attr.xml enum values
 * 
 */
public enum FontConstants {
	SourceSansProLight("fonts/SourceSansPro-Light.ttf", 0),
	SourceSansProRegular("fonts/SourceSansPro-Regular.ttf", 1),
	SourceSansProSemibold("fonts/SourceSansPro-Semibold.ttf",2);


	private String fontPathName;
	private int fontValue;

	private FontConstants(String fontPathName, int fontValue) {
		this.fontPathName = fontPathName;
		this.fontValue = fontValue;
	}

	/**
	 * 
	 * @param value
	 * @return Returns font's file location existing in assets folder by comparing with Font value. Return's default font "ProximaNova-Regular.otf" if fontValue not exist .
	 * 
	 */
	public static String getFontNameFromFontValue(int value) {

		for (FontConstants font : FontConstants.values()) {
			if (font.getFontValue() == value)
				return font.getFontPathName();
		}

		return SourceSansProRegular.getFontPathName();
	}

	public int getFontValue() {
		return fontValue;
	}

	public String getFontPathName() {
		return fontPathName;
	}
}