package com.bv.commonlibrary.constants;

import android.os.Environment;

/**
 * Created by Administrator on 5/3/2016.
 */
public class CommonConstants {

    /* Storage files directory */
    public static String APP_HOME = Environment.getExternalStorageDirectory()
            .getPath() + "/Mile2Smile";
    public static String DIR_LOG = APP_HOME + "/Log";
    public static String LOG_ZIP = APP_HOME + "/Mile2Smile.zip";
    public static String DIR_IMAGES = APP_HOME + "/data";

    public static String PREF_FILE = "PREF_UtilLib";

}
