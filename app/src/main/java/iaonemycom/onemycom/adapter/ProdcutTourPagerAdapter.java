package iaonemycom.onemycom.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;

import iaonemycom.onemycom.R;

/**
 * Created by jay on 23/6/16.
 */
public class ProdcutTourPagerAdapter extends PagerAdapter {
    private Context mContext;
    private ArrayList<Integer> mList;
    public ProdcutTourPagerAdapter(Context mContext, ArrayList<Integer> _List) {
        this.mContext = mContext;
        mList = _List;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);

    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_tour_pager, container, false);
        ImageView imgView = (ImageView) itemView.findViewById(R.id.imgView);
//        imgView.setImageResource(mList.get(position));
        imgView.setBackgroundResource(mList.get(position
        ));
//        FancyTextview txtvalue = (FancyTextview) itemView.findViewById(R.id.txtvalue);
//        txtvalue.setText(mList.get(position));
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}