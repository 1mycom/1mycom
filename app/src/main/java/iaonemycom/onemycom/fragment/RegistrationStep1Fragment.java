package iaonemycom.onemycom.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bv.commonlibrary.custom.FancyButton;
import com.bv.commonlibrary.custom.FancyEditText;
import com.bv.commonlibrary.util.Pref;
import com.bv.commonlibrary.util.Utils;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import iaonemycom.onemycom.R;
import iaonemycom.onemycom.activity.RegistrationStepActivity;
import iaonemycom.onemycom.constants.Constants;
import iaonemycom.onemycom.util.ActivityNavigationEnum;
import iaonemycom.onemycom.util.StoreCodeEnum;

/**
 * Created by jay on 9/3/17.
 */

public class RegistrationStep1Fragment extends Fragment {

    Context mContext;

    @BindView(R.id.FirstName)
    FancyEditText edt_firstname;

    @BindView(R.id.LastName)
    FancyEditText edt_lastname;

    @BindView(R.id.Email)
    FancyEditText edt_email;

    @BindView(R.id.cnf_Email)
    FancyEditText edt_cnf_email;

    @BindView(R.id.register_password)
    FancyEditText edt_password;

    @BindView(R.id.cnf_password)
    FancyEditText edt_cnf_password;

    @BindView(R.id.btnNext)
    FancyButton btnNext;

    @BindView(R.id.imageBack)
    ImageView imageBack;

    @BindView(R.id.relParent)
    RelativeLayout relParent;

    String firstname,lastname,email,email_cnfm,password,cnf_password="";


    ActivityNavigationEnum fromWhereEnum;
    public RegistrationStep1Fragment()
    {

    }
    public static RegistrationStep1Fragment newInstance(int fromWhere ) {
        RegistrationStep1Fragment fragment = new RegistrationStep1Fragment();
        Bundle args = new Bundle();
        args.putInt("fromWhereEnum", fromWhere);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            fromWhereEnum = ActivityNavigationEnum.values()[getArguments().getInt("fromWhereEnum")];
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_registration_step1, null);

        mContext = getActivity();
        ButterKnife.bind(this, view);
        Pref.openPref(mContext);
//        Utils.setStatusBarColorFromFragment(getActivity(), R.color.screen_background_dark_transparent);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        imageBack.setVisibility(View.VISIBLE);

        Utils.setupOutSideTouchHideKeyboard(relParent, mContext);
        return view;
    }

    @OnClick(R.id.imageBack)
    public void backButton(View view)
    {
        getActivity().finish();
        getActivity().overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }
    @OnClick(R.id.btnNext)
    public void nextButton(View view)
    {
        firstname = edt_firstname.getText().toString().trim();
        lastname = edt_lastname.getText().toString().trim();
        email = edt_email.getText().toString().trim();
        email_cnfm = edt_cnf_email.getText().toString().trim();
        password = edt_password.getText().toString().trim();
        cnf_password = edt_cnf_password.getText().toString().trim();

        if(firstname.length()==0)
        {
            edt_firstname.setError(getResources().getString(R.string.empty));
        }
        else if (lastname.length()==0)
        {
            edt_lastname.setError(getResources().getString(R.string.empty));
        }
        else  if (!Utils.isEmailValid(email))
        {
            // Toast.makeText(getApplicationContext(),"VALID EMAIL FORMAT",Toast.LENGTH_LONG).show();
            edt_email.setError(getResources().getString(R.string.invalidemail));
        }else if (!email_cnfm.equals(email))
        {
            edt_cnf_email.setError(getResources().getString(R.string.alert_confirmnotmatch_email));
        }
        else if (password.length()<8)
        {
            edt_password.setError(getResources().getString(R.string.minimum_8_character));
        }
        else if (!cnf_password.equals(password))
        {
            edt_cnf_password.setError(getResources().getString(R.string.confirmnotmatch));
        }
        else
        {
            HashMap<String,String> hashMap = new HashMap<>();
            hashMap.put("firstname",firstname);
            hashMap.put("lastname",lastname);
            hashMap.put("email",email);
            hashMap.put("password",password);
            hashMap.put("store_id", StoreCodeEnum.SPAIN_STORE.getValue());
//            hashMap.put("cart_id", management.getCartId());


            redirect(hashMap);

//            Intent popActivity = new Intent(mContext, RegistrationStep2Activity.class);
//            popActivity.putExtra("stepone_data", hashMap);
//            startActivity(popActivity);
//            getActivity().overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

        }
    }

    private void redirect(HashMap<String, String> hashMap)
    {
        switch (fromWhereEnum)
        {
            case FROM_LOGIN: ((RegistrationStepActivity)getActivity()).addFragments(Constants.TAG_FRAGMENT_REG_STEP2,R.id.frameLayout,
                    RegistrationStep2Fragment.newInstance(hashMap, fromWhereEnum.getValue()), true);
                break;

            case FROM_CART:
                break;

            case FROM_MYCARD:
                break;

        }
    }
}
