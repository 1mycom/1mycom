package iaonemycom.onemycom.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bv.commonlibrary.custom.FancyButton;
import com.bv.commonlibrary.custom.FancyEditText;
import com.bv.commonlibrary.custom.FancyTextview;
import com.bv.commonlibrary.http.HttpFormRequest;
import com.bv.commonlibrary.util.JSONHandler;
import com.bv.commonlibrary.util.Pref;
import com.bv.commonlibrary.util.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import iaonemycom.onemycom.R;
import iaonemycom.onemycom.activity.MainActivity;
import iaonemycom.onemycom.activity.RegistrationStepActivity;
import iaonemycom.onemycom.constants.URLConstants;
import iaonemycom.onemycom.model.LoginBean;
import iaonemycom.onemycom.util.ActivityNavigationEnum;
import iaonemycom.onemycom.util.PrefKey;
import iaonemycom.onemycom.util.StoreCodeEnum;
import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * Created by jay on 9/3/17.
 */

public class RegistrationStep2Fragment extends Fragment {

    Context mContext;

    @BindView(R.id.btnRegisterNow)
    FancyButton btnRegister;

    @BindView(R.id.spinnerCountry)
    FancyEditText spCountry;

    @BindView(R.id.spinnerState)
    FancyEditText spState;

    @BindView(R.id.spinnerCity)
    FancyEditText spCity;

    @BindView(R.id.edtStreet)
    FancyEditText mEt_addr;

    @BindView(R.id.edtZip)
    FancyEditText mEt_zip;

    @BindView(R.id.edtPhone)
    FancyEditText mEt_phone;

    @BindView(R.id.viewCheck)
    CheckBox mChckView;

    @BindView(R.id.tv_terms_link)
    FancyTextview tv_terms_link;

    @BindView(R.id.imgBack)
    ImageView imageBack;

    @BindView(R.id.relParent)
    RelativeLayout relParent;

    String jstring, mSelCountry = "", mSelState = "", mSelCity = "";

    ArrayList<String> countrycodelist = new ArrayList<>();
    ArrayList<String> countrylabellist = new ArrayList<>();
    ArrayList<String> statelabellist = new ArrayList<>();
    ArrayList<String> statecodelist = new ArrayList<>();
    ArrayList<String> citylist = new ArrayList<>();
    ArrayList<String> citycodelist = new ArrayList<>();
    boolean canCheck = false;
    PopupWindow popupWindow;
    private int mPopupWindowWidth = WindowManager.LayoutParams.WRAP_CONTENT;
    HashMap<String, String> hashMap_stored = new HashMap<>();
    private HttpFormRequest mAuthTask;

    int fromWhere=1;
    public static RegistrationStep2Fragment newInstance( HashMap<String, String> hashMap, int fromWhere) {
        RegistrationStep2Fragment fragment = new RegistrationStep2Fragment();
        Bundle args = new Bundle();
        args.putSerializable("stepone_data", hashMap);
        args.putInt("fromWhereEnum", fromWhere);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            hashMap_stored = (HashMap<String, String>) getArguments().getSerializable("stepone_data");
            fromWhere = getArguments().getInt("fromWhereEnum");
        }
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_registration_step2, null);

        mContext = getActivity();
        ButterKnife.bind(this, view);


        Pref.openPref(mContext);
//        Utils.setStatusBarColorFromFragment(getActivity(), R.color.screen_background_dark_transparent);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        imageBack.setVisibility(View.VISIBLE);

        Utils.setupOutSideTouchHideKeyboard(relParent, mContext);

//        Utils.setStatusBarColor(this, R.color.blue_statusbar);

        spCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (countrylabellist.size() > 0) {
                    populateData(R.string.Country);
                } else {
                    getCountryList();
                }
            }
        });
        spState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (statelabellist.size() > 0) {
                    populateData(R.string.State);
                } else {
                    if (!mSelCountry.equals("")) {
                        getStateList(mSelCountry);
                    } else {
                        Toast.makeText(mContext, getResources().getString(R.string.alert_country), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
        spCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (citylist.size() > 0) {
                    populateData(R.string.City);
                } else {
                    if (!mSelState.equals("")) {
                        getCityList(mSelState);
                    } else {
                        Toast.makeText(mContext, getResources().getString(R.string.alert_state), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });


        mChckView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    canCheck = true;
                } else {
                    canCheck = false;
                }
            }
        });
        spCountry.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {

                    @SuppressWarnings("deprecation")
                    @Override
                    public void onGlobalLayout() {
                        // TODO Auto-generated method stub
                        spCountry.getViewTreeObserver()
                                .removeGlobalOnLayoutListener(this);
                        mPopupWindowWidth = spCountry.getMeasuredWidth();

                    }
                });

        Utils.setupOutSideTouchHideKeyboard(relParent, mContext);
        return view;
    }

    @OnClick(R.id.imageBack)
    public void onBackClick(View view) {

        ActivityNavigationEnum fromRedirect = ActivityNavigationEnum.values()[fromWhere];
        switch (fromRedirect)
        {
            case FROM_LOGIN: ((RegistrationStepActivity)getActivity()).popFragments();
                break;
            case FROM_CART:
                break;
        }
    }


    @OnClick(R.id.btnRegisterNow)
    public void onRegisterNowClick(View view) {
        if (mSelCountry.length() == 0) {
            Toast.makeText(mContext, getResources().getString(R.string.alert_country), Toast.LENGTH_LONG).show();
        } else if (mSelState.length() == 0) {
            Toast.makeText(mContext, getResources().getString(R.string.alert_state), Toast.LENGTH_LONG).show();
        } else if (mSelCity.length() == 0) {
            Toast.makeText(mContext, getResources().getString(R.string.alert_city), Toast.LENGTH_LONG).show();
        } else if (mEt_addr.length() == 0) {
            mEt_addr.setError(getResources().getString(R.string.empty));
        } else if (mEt_zip.length() == 0) {
            mEt_zip.setError(getResources().getString(R.string.empty));
        } else if (mEt_phone.length() == 0) {
            mEt_phone.setError(getResources().getString(R.string.empty));
        } else if (!canCheck) {
            Toast.makeText(mContext, getResources().getString(R.string.alert_age), Toast.LENGTH_LONG).show();
        } else {

            if(Utils.isOnline(mContext)) {

                RequestBody mReqBody = new FormBody.Builder()
                        .add("firstname", hashMap_stored.get("firstname"))
                        .add("lastname", hashMap_stored.get("lastname"))
                        .add("email", hashMap_stored.get("email"))
                        .add("password", hashMap_stored.get("password"))
                        .add("street", mEt_addr.getText().toString().trim())
                        .add("city", mSelCity)
                        .add("region", mSelState)
                        .add("postcode", mEt_zip.getText().toString().trim())
                        .add("telephone", mEt_phone.getText().toString().trim())
                        .add("postcode", mEt_zip.getText().toString().trim())
                        .add("country", mSelCountry)
                        .add("store_id", StoreCodeEnum.SPAIN_STORE.getValue())
                        .build();

                mAuthTask = new HttpFormRequest(mContext, URLConstants.registerUrl, "",
                        mReqBody, true, new HttpFormRequest.AsyncTaskCompleteListener() {
                    @Override
                    public void asyncTaskComplted(String response) {
                        mAuthTask = null;
                        try {
                            if (response != null) {
                                JSONObject jsonObj = new JSONObject(response);

                                JSONObject jsonData = jsonObj.getJSONObject("data");
                                JSONArray jCustomer = jsonData.getJSONArray("customer");

                                JSONObject mLoginResponse = jCustomer.getJSONObject(0);

                                if(mLoginResponse.getString("status").equalsIgnoreCase("success")) {

                                    LoginBean mBean = (LoginBean) new JSONHandler()
                                            .parse(mLoginResponse.toString(), LoginBean.class,
                                                    "iaonemycom.onemycom.model");

                                    Pref.setBeanValue(mContext, PrefKey.KEY_LOGIN_RESPONSE, mBean);
                                    Pref.setValue(mContext, PrefKey.KEY_CUSTOMERID, mBean.getCustomer_id());
                                    redirect();

                                } else {
                                    Utils.showToast(mContext, getString(R.string.msg_itSeems));
                                }

                            } else {
                                Utils.showToast(mContext, getString(R.string.msg_itSeems));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Utils.showToast(mContext, getString(R.string.msg_itSeems));
                        }
                    }
                });
                mAuthTask.execute();
            }
            else {
                Utils.showToast(mContext, getString(R.string.msg_noInternet));
            }
        }
    }

    /**
     * This method is used to get Country List
     */
    private void getCountryList() {
        try {
            if (Utils.isOnline(mContext)) {
                mAuthTask = new HttpFormRequest(mContext, URLConstants.countryUrl, "",
                        null, true, new HttpFormRequest.AsyncTaskCompleteListener() {
                    @Override
                    public void asyncTaskComplted(String response) {
                        mAuthTask = null;
                        try {
                            if (response != null) {

                                JSONObject jsonObject = new JSONObject(response);
//
                                JSONObject jsonObjectData = jsonObject.getJSONObject("data");

                                JSONArray jsonArray = jsonObjectData.getJSONArray("country");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    countrycodelist.add(object.getString("country_id"));
                                    countrylabellist.add(object.getString("name"));
                                }
//
                                populateData(R.string.Country);
                            } else {
//                                Utils.showToast(RegistrationStep2Activity.this, getString(R.string.msg_itSeems));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                mAuthTask.execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * This method is used to get State list
     */
    private void getStateList(String country_code) {
        //call API

        RequestBody mReqBody = new FormBody.Builder()
                .add("country", country_code)
                .build();

        try {
            if (Utils.isOnline(mContext)) {
                mAuthTask = new HttpFormRequest(mContext, URLConstants.stateUrl, "",
                        mReqBody, true, new HttpFormRequest.AsyncTaskCompleteListener() {
                    @Override
                    public void asyncTaskComplted(String response) {
                        mAuthTask = null;
                        try {
                            if (response != null) {

                                JSONObject jsonObject = new JSONObject(response);
//
                                JSONObject jsonObjectData = jsonObject.getJSONObject("data");

                                JSONArray jsonArray = jsonObjectData.getJSONArray("region");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    statecodelist.add(object.getString("region_id"));
                                    statelabellist.add(object.getString("name"));
                                }
                                populateData(R.string.State);
                            } else {
//                                Utils.showToast(RegistrationStep2Activity.this, getString(R.string.msg_itSeems));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                mAuthTask.execute();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * This method is used to get City List
     */
    private void getCityList(String region_id) {
        //call API

        RequestBody mReqBody = new FormBody.Builder()
                .add("region", region_id)
                .build();
        try {
            if (Utils.isOnline(mContext)) {
                mAuthTask = new HttpFormRequest(mContext, URLConstants.cityUrl, "",
                        mReqBody, true, new HttpFormRequest.AsyncTaskCompleteListener() {
                    @Override
                    public void asyncTaskComplted(String response) {
                        mAuthTask = null;
                        try {
                            if (response != null) {

                                JSONObject jsonObject = new JSONObject(response);
//
                                JSONObject jsonObjectData = jsonObject.getJSONObject("data");

                                JSONArray jsonArray = jsonObjectData.getJSONArray("city");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    citycodelist.add(object.getString("value"));
                                    citylist.add(object.getString("label"));
                                }
                                populateData(R.string.City);
                            } else {
//                                Utils.showToast(RegistrationStep2Activity.this, getString(R.string.msg_itSeems));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                mAuthTask.execute();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /** Method to bind value to Dropdown*/
    public void populateData(int type) {

        switch (type) {
            case R.string.Country:
                // Add Data to country Spinner
                if (countrylabellist != null && countrylabellist.size() > 0) {

                    PopupWindow popUp = popupWindowsort(countrylabellist, type);
                    popUp.showAsDropDown(spCountry, 0, 0); //

                } else {
                    Toast.makeText(mContext, getResources().getString(R.string.alert_nodata), Toast.LENGTH_LONG).show();
                }
                break;
            case R.string.State:
                // Add Data to country Spinner
                if (statelabellist != null && statelabellist.size() > 0) {

                    PopupWindow popUp = popupWindowsort(statelabellist, type);
                    popUp.showAsDropDown(spState, 0, 0); //

                } else {
                    Toast.makeText(mContext, getResources().getString(R.string.alert_nodata), Toast.LENGTH_LONG).show();
                }
                break;
            case R.string.City:
                // Add Data to country Spinner
                if (citylist != null && citylist.size() > 0) {

                    PopupWindow popUp = popupWindowsort(citylist, type);
                    popUp.showAsDropDown(spCity, 0, 0); //

                } else {
                    Toast.makeText(mContext, getResources().getString(R.string.alert_nodata), Toast.LENGTH_LONG).show();
                }
                break;
        }

    }

    private PopupWindow popupWindowsort(final ArrayList<String> contentArray, final int type) {

        // initialize a pop up window type
        popupWindow = new PopupWindow(mContext);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1,
                contentArray);
        // the drop down list is a list view
        ListView listViewSort = new ListView(mContext);//(ListView)layout.findViewById(R.id.listView);
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        param.setMargins(16, 0, 16, 0);
        listViewSort.setLayoutParams(param);
        listViewSort.setSmoothScrollbarEnabled(true);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
            listViewSort.setScrollBarSize(5);
        }
        // set our adapter and pass our pop up window contents
        listViewSort.setAdapter(adapter);

        // set on item selected
        listViewSort.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                switch (type) {
                    case R.string.Country:
                        statecodelist.clear();
                        statelabellist.clear();
                        citycodelist.clear();
                        citylist.clear();
                        mSelState = "";
                        spState.setText("");
                        spCity.setText("");
                        spCountry.setText(countrylabellist.get(position));
                        mSelCountry = countrycodelist.get(position);
                        break;
                    case R.string.State:
                        citycodelist.clear();
                        citylist.clear();
                        mSelCity = "";
                        spCity.setText("");
                        spState.setText(statelabellist.get(position));
                        mSelState = statecodelist.get(position);
                        break;
                    case R.string.City:
                        spCity.setText(citylist.get(position));
                        mSelCity = citycodelist.get(position);
                        break;
                }

                if (popupWindow != null) {
                    popupWindow.dismiss();
                }
            }
        });

        // some other visual settings for popup window
        popupWindow.setFocusable(true);
        popupWindow.setWidth(mPopupWindowWidth);
        popupWindow.setBackgroundDrawable(getResources().getDrawable(R.drawable.popup_background));
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        // set the listview as popup content
        popupWindow.setContentView(listViewSort);

        return popupWindow;
    }

    private void redirect()
    {
        ActivityNavigationEnum fromRedirect = ActivityNavigationEnum.values()[fromWhere];
        switch (fromRedirect)
        {
            case FROM_LOGIN:
                Intent intent = new Intent(mContext, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivityForResult(intent,1001);
                getActivity().finish();
                break;

            case FROM_CART:
                break;

            case FROM_MYCARD:
                break;

        }
    }
}
