package iaonemycom.onemycom.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ScrollView;

import com.bv.commonlibrary.custom.FancyButton;
import com.bv.commonlibrary.custom.FancyEditText;
import com.bv.commonlibrary.custom.FancyTextview;
import com.bv.commonlibrary.http.HttpFormRequest;
import com.bv.commonlibrary.util.JSONHandler;
import com.bv.commonlibrary.util.Pref;
import com.bv.commonlibrary.util.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import iaonemycom.onemycom.R;
import iaonemycom.onemycom.activity.MainActivity;
import iaonemycom.onemycom.activity.RegistrationStepActivity;
import iaonemycom.onemycom.constants.URLConstants;
import iaonemycom.onemycom.model.LoginBean;
import iaonemycom.onemycom.util.PrefKey;
import iaonemycom.onemycom.util.StoreCodeEnum;
import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * Created by jay on 9/3/17.
 */

public class LoginFragment extends Fragment
{
    @BindView(R.id.scrollView)
    ScrollView mScrollView;

    @BindView(R.id.registration)
    FancyTextview txtRegistration;

    @BindView(R.id.forgotPassword)
    FancyTextview txtForgot_Password;

    @BindView(R.id.txtWelcome)
    FancyTextview txtWelcome;

    @BindView(R.id.user_name)
    FancyEditText edt_username;

    @BindView(R.id.usr_password)
    FancyEditText edt_password;

    @BindView(R.id.Login)
    FancyButton Login_button;

    @BindView(R.id.btn_skip_registration)
    FancyButton btn_skip_registration;


    String username,password;
    private HttpFormRequest mAuthTask;

    Context mContext;
    @Override
    public void onStart() {
        super.onStart();
    }

    public LoginFragment() {
    }
    public static LoginFragment newInstance( ) {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_login, null);

        mContext = getActivity();
        ButterKnife.bind(this, view);
        Pref.openPref(mContext);
//        Utils.setStatusBarColorFromFragment(getActivity(), R.color.screen_background_dark_transparent);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        return view;
    }


    @OnClick(R.id.forgotPassword)
    public void onForgotPasswordClick(View view)
    {
//        Intent forgotpwdIntent = new Intent(getApplicationContext(), ForgotPassword.class);
//                startActivity(forgotpwdIntent);
    }

    @OnClick(R.id.registration)
    public void onRegisterNowClick(View view)
    {
        Intent intent = new Intent(mContext, RegistrationStepActivity.class);
        startActivityForResult(intent,1001);
        getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    @OnClick(R.id.btn_skip_registration)
    public void onRegistrationSkipClick(View view)
    {
        Intent intent = new Intent(mContext, MainActivity.class);
        startActivity(intent);
        getActivity().finish();
        getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    @OnClick(R.id.Login)
    public void onSignInClick(View view)
    {
        username=edt_username.getText().toString().trim();
        password=edt_password.getText().toString().trim();

        if(!Utils.isNotNull(username))
        {
            edt_username.setError(getResources().getString(R.string.empty));
            return;
        }
        else if (!Utils.isEmailValid(username))
        {
            edt_username.setError(getResources().getString(R.string.invalidemail));
            return;
        }
        else if(!Utils.isNotNull(password))
        {
            edt_password.setError(getResources().getString(R.string.empty));
            return;
        }
        else
        {
            if(Utils.isOnline(mContext)) {

                RequestBody mReqBody = new FormBody.Builder()
                        .add("email", username)
                        .add("password", password)
                        .add("store_id", StoreCodeEnum.SPAIN_STORE.getValue())
                        .build();

                mAuthTask = new HttpFormRequest(mContext, URLConstants.customerLogin, "",
                        mReqBody, true, new HttpFormRequest.AsyncTaskCompleteListener() {
                    @Override
                    public void asyncTaskComplted(String response) {
                        mAuthTask = null;
                        try {
                            if (response != null) {
                                JSONObject jsonObj = new JSONObject(response);

                                JSONObject jsonData = jsonObj.getJSONObject("data");
                                JSONArray jCustomer = jsonData.getJSONArray("customer");

                                JSONObject mLoginResponse = jCustomer.getJSONObject(0);

                                if(mLoginResponse.getString("status").equalsIgnoreCase("success")) {

                                    LoginBean mBean = (LoginBean) new JSONHandler()
                                            .parse(mLoginResponse.toString(), LoginBean.class,
                                                    "iaonemycom.onemycom.model");

                                    Pref.setBeanValue(mContext, PrefKey.KEY_LOGIN_RESPONSE, mBean);
                                    Pref.setValue(mContext, PrefKey.KEY_CUSTOMERID, mBean.getCustomer_id());

                                    Intent intent = new Intent(mContext, MainActivity.class);
                                    startActivity(intent);
                                    getActivity().finish();
                                    getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                                }
                                else
                                {
                                    Utils.showToast(mContext, mLoginResponse.getString("message"));
                                }

                            } else {
                                Utils.showToast(mContext, getString(R.string.msg_itSeems));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Utils.showToast(mContext, getString(R.string.msg_itSeems));
                        }
                    }
                });
                mAuthTask.execute();
            }
            else {
                Utils.showToast(mContext, getString(R.string.msg_noInternet));
            }
        }
    }
}
