package iaonemycom.onemycom.util;

/**
 * Created by jay on 8/3/17.
 */

public enum ActivityNavigationEnum {
    FROM_LOGIN(0), FROM_CART(1), FROM_MYCARD(2);
    private final int value;

    ActivityNavigationEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
