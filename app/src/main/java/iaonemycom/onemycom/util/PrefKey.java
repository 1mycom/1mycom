package iaonemycom.onemycom.util;

/**
 * Created by jay on 13/6/16.
 */
public class PrefKey {
    public static String KEY_USERID = "userID";
    public static String KEY_CAN_SHOW_WELCOME = "canShowWelcome";

    public static String KEY_LOGIN_RESPONSE = "loginResponse";
    public static String KEY_CUSTOMERID = "customerID";

    public static  final String cmsaddon="CMS";
    public static  final String socialloginaddon="SOCIALLOGIN";
    public static  final String wishlistaddon="WISHLIST";
    public static  final String checkoutaddon="CHECKOUT";
    public static  final String dealsaddon="DEALS";
    public static  final String reviewaddon="REVIEW";
    public static  final String storeaddon="STORE";
    public static  final String notificationaddon="NOTIFICATION";
    public static  final String Extension="Extension";

}
