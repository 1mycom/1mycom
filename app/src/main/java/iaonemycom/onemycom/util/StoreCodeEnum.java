package iaonemycom.onemycom.util;

/**
 * Created by jay on 8/3/17.
 */

public enum StoreCodeEnum {
    SPAIN_STORE("b2c_spanish"), PORTUGUESE_STORE("b2c_portuguese"), ENGLISH_STORE("b2c_english");
    private final String value;

    StoreCodeEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
