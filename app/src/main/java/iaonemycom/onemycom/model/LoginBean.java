package iaonemycom.onemycom.model;

import java.io.Serializable;

/**
 * Created by jay on 8/3/17.
 */

public class LoginBean implements Serializable {

    private String name;
    private String telephone;
    private String email;
    private String customer_id;
    private String hash;
    private int cart_summary;
    private String status;
    private String mycom_balance;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public int getCart_summary() {
        return cart_summary;
    }

    public void setCart_summary(int cart_summary) {
        this.cart_summary = cart_summary;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMycom_balance() {
        return mycom_balance;
    }

    public void setMycom_balance(String mycom_balance) {
        this.mycom_balance = mycom_balance;
    }
}
