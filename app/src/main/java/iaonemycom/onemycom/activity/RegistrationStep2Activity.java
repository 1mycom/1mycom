//package iaonemycom.onemycom.activity;
//
//
//import android.content.Intent;
//import android.os.Build;
//import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.ViewTreeObserver;
//import android.view.WindowManager;
//import android.widget.AdapterView;
//import android.widget.ArrayAdapter;
//import android.widget.CheckBox;
//import android.widget.CompoundButton;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.ListView;
//import android.widget.PopupWindow;
//import android.widget.RelativeLayout;
//import android.widget.Toast;
//
//import com.bv.commonlibrary.custom.FancyButton;
//import com.bv.commonlibrary.custom.FancyEditText;
//import com.bv.commonlibrary.custom.FancyTextview;
//import com.bv.commonlibrary.http.HttpFormRequest;
//import com.bv.commonlibrary.util.JSONHandler;
//import com.bv.commonlibrary.util.Pref;
//import com.bv.commonlibrary.util.Utils;
//
//import org.json.JSONArray;
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
//import iaonemycom.onemycom.R;
//import iaonemycom.onemycom.constants.URLConstants;
//import iaonemycom.onemycom.model.LoginBean;
//import iaonemycom.onemycom.util.PrefKey;
//import iaonemycom.onemycom.util.StoreCodeEnum;
//import okhttp3.FormBody;
//import okhttp3.RequestBody;
//
///**
// */
//public class RegistrationStep2Activity extends AppCompatActivity {
//
//    @BindView(R.id.btnRegisterNow)
//    FancyButton btnRegister;
//
//    @BindView(R.id.spinnerCountry)
//    FancyEditText spCountry;
//
//    @BindView(R.id.spinnerState)
//    FancyEditText spState;
//
//    @BindView(R.id.spinnerCity)
//    FancyEditText spCity;
//
//    @BindView(R.id.edtStreet)
//    FancyEditText mEt_addr;
//
//    @BindView(R.id.edtZip)
//    FancyEditText mEt_zip;
//
//    @BindView(R.id.edtPhone)
//    FancyEditText mEt_phone;
//
//    @BindView(R.id.viewCheck)
//    CheckBox mChckView;
//
//    @BindView(R.id.tv_terms_link)
//    FancyTextview tv_terms_link;
//
//    @BindView(R.id.imgBack)
//    ImageView imageBack;
//
//    @BindView(R.id.relParent)
//    RelativeLayout relParent;
//
//    String jstring, mSelCountry = "", mSelState = "", mSelCity = "";
//
//    ArrayList<String> countrycodelist = new ArrayList<>();
//    ArrayList<String> countrylabellist = new ArrayList<>();
//    ArrayList<String> statelabellist = new ArrayList<>();
//    ArrayList<String> statecodelist = new ArrayList<>();
//    ArrayList<String> citylist = new ArrayList<>();
//    ArrayList<String> citycodelist = new ArrayList<>();
//    boolean canCheck = false;
//    PopupWindow popupWindow;
//    private int mPopupWindowWidth = WindowManager.LayoutParams.WRAP_CONTENT;
//    HashMap<String, String> hashMap_stored = new HashMap<>();
//    private HttpFormRequest mAuthTask;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_registration_step2);
//        ButterKnife.bind(this);
//
//
//        hashMap_stored = (HashMap<String, String>) getIntent().getSerializableExtra("stepone_data");
//
//
//        imageBack.setVisibility(View.VISIBLE);
//
//        Utils.setupOutSideTouchHideKeyboard(relParent, RegistrationStep2Activity.this);
//
//        Utils.setStatusBarColor(this, R.color.blue_statusbar);
//
//        spCountry.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (countrylabellist.size() > 0) {
//                    populateData(R.string.Country);
//                } else {
//                    getCountryList();
//                }
//            }
//        });
//        spState.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (statelabellist.size() > 0) {
//                    populateData(R.string.State);
//                } else {
//                    if (!mSelCountry.equals("")) {
//                        getStateList(mSelCountry);
//                    } else {
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.alert_country), Toast.LENGTH_LONG).show();
//                    }
//                }
//            }
//        });
//        spCity.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (citylist.size() > 0) {
//                    populateData(R.string.City);
//                } else {
//                    if (!mSelState.equals("")) {
//                        getCityList(mSelState);
//                    } else {
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.alert_state), Toast.LENGTH_LONG).show();
//                    }
//                }
//            }
//        });
//
//
//        mChckView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//                    canCheck = true;
//                } else {
//                    canCheck = false;
//                }
//            }
//        });
//        spCountry.getViewTreeObserver().addOnGlobalLayoutListener(
//                new ViewTreeObserver.OnGlobalLayoutListener() {
//
//                    @SuppressWarnings("deprecation")
//                    @Override
//                    public void onGlobalLayout() {
//                        // TODO Auto-generated method stub
//                        spCountry.getViewTreeObserver()
//                                .removeGlobalOnLayoutListener(this);
//                        mPopupWindowWidth = spCountry.getMeasuredWidth();
//
//                    }
//                });
//
//    }
//
//    @OnClick(R.id.imageBack)
//    public void onBackClick(View view) {
//        finish();
//    }
//
//
//    @OnClick(R.id.btnRegisterNow)
//    public void onRegisterNowClick(View view) {
//        if (mSelCountry.length() == 0) {
//            Toast.makeText(getApplicationContext(), getResources().getString(R.string.alert_country), Toast.LENGTH_LONG).show();
//        } else if (mSelState.length() == 0) {
//            Toast.makeText(getApplicationContext(), getResources().getString(R.string.alert_state), Toast.LENGTH_LONG).show();
//        } else if (mSelCity.length() == 0) {
//            Toast.makeText(getApplicationContext(), getResources().getString(R.string.alert_city), Toast.LENGTH_LONG).show();
//        } else if (mEt_addr.length() == 0) {
//            mEt_addr.setError(getResources().getString(R.string.empty));
//        } else if (mEt_zip.length() == 0) {
//            mEt_zip.setError(getResources().getString(R.string.empty));
//        } else if (mEt_phone.length() == 0) {
//            mEt_phone.setError(getResources().getString(R.string.empty));
//        } else if (!canCheck) {
//            Toast.makeText(getApplicationContext(), getResources().getString(R.string.alert_age), Toast.LENGTH_LONG).show();
//        } else {
//
//            if(Utils.isOnline(this)) {
//
//                RequestBody mReqBody = new FormBody.Builder()
//                        .add("firstname", hashMap_stored.get("firstname"))
//                        .add("lastname", hashMap_stored.get("lastname"))
//                        .add("email", hashMap_stored.get("email"))
//                        .add("password", hashMap_stored.get("password"))
//                        .add("street", mEt_addr.getText().toString().trim())
//                        .add("city", mSelCity)
//                        .add("region", mSelState)
//                        .add("postcode", mEt_zip.getText().toString().trim())
//                        .add("telephone", mEt_phone.getText().toString().trim())
//                        .add("postcode", mEt_zip.getText().toString().trim())
//                        .add("country", mSelCountry)
//                        .add("store_id", StoreCodeEnum.SPAIN_STORE.getValue())
//                        .build();
//
//                mAuthTask = new HttpFormRequest(this, URLConstants.registerUrl, "",
//                        mReqBody, true, new HttpFormRequest.AsyncTaskCompleteListener() {
//                    @Override
//                    public void asyncTaskComplted(String response) {
//                        mAuthTask = null;
//                        try {
//                            if (response != null) {
//                                    JSONObject jsonObj = new JSONObject(response);
//
//                                    JSONObject jsonData = jsonObj.getJSONObject("data");
//                                    JSONArray jCustomer = jsonData.getJSONArray("customer");
//
//                                    JSONObject mLoginResponse = jCustomer.getJSONObject(0);
//
//                                    if(mLoginResponse.getString("status").equalsIgnoreCase("success")) {
//
//                                        LoginBean mBean = (LoginBean) new JSONHandler()
//                                                .parse(mLoginResponse.toString(), LoginBean.class,
//                                                        "iaonemycom.onemycom.model");
//
//                                        Pref.setBeanValue(RegistrationStep2Activity.this, PrefKey.KEY_LOGIN_RESPONSE, mBean);
//
//                                        Intent intent = new Intent(RegistrationStep2Activity.this, MainActivity.class);
//                                        startActivity(intent);
//                                        finish();
//                                } else {
//                                    Utils.showToast(RegistrationStep2Activity.this, getString(R.string.msg_itSeems));
//                                }
//
//                            } else {
//                                Utils.showToast(RegistrationStep2Activity.this, getString(R.string.msg_itSeems));
//                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                            Utils.showToast(RegistrationStep2Activity.this, getString(R.string.msg_itSeems));
//                        }
//                    }
//                });
//                mAuthTask.execute();
//            }
//            else {
//                Utils.showToast(this, getString(R.string.msg_noInternet));
//            }
//        }
//    }
//
//    /**
//     * This method is used to get Country List
//     */
//    private void getCountryList() {
//        try {
//            if (Utils.isOnline(this)) {
//                mAuthTask = new HttpFormRequest(this, URLConstants.countryUrl, "",
//                        null, true, new HttpFormRequest.AsyncTaskCompleteListener() {
//                    @Override
//                    public void asyncTaskComplted(String response) {
//                        mAuthTask = null;
//                        try {
//                            if (response != null) {
//
//                                JSONObject jsonObject = new JSONObject(response);
////
//                                JSONObject jsonObjectData = jsonObject.getJSONObject("data");
//
//                                JSONArray jsonArray = jsonObjectData.getJSONArray("country");
//                                for (int i = 0; i < jsonArray.length(); i++) {
//                                    JSONObject object = jsonArray.getJSONObject(i);
//                                    countrycodelist.add(object.getString("country_id"));
//                                    countrylabellist.add(object.getString("name"));
//                                }
////
//                                populateData(R.string.Country);
//                            } else {
////                                Utils.showToast(RegistrationStep2Activity.this, getString(R.string.msg_itSeems));
//                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                });
//                mAuthTask.execute();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    /**
//     * This method is used to get State list
//     */
//    private void getStateList(String country_code) {
//        //call API
//
//        RequestBody mReqBody = new FormBody.Builder()
//                .add("country", country_code)
//                .build();
//
//        try {
//            if (Utils.isOnline(this)) {
//                mAuthTask = new HttpFormRequest(this, URLConstants.stateUrl, "",
//                        mReqBody, true, new HttpFormRequest.AsyncTaskCompleteListener() {
//                    @Override
//                    public void asyncTaskComplted(String response) {
//                        mAuthTask = null;
//                        try {
//                            if (response != null) {
//
//                                JSONObject jsonObject = new JSONObject(response);
////
//                                JSONObject jsonObjectData = jsonObject.getJSONObject("data");
//
//                                JSONArray jsonArray = jsonObjectData.getJSONArray("region");
//                                for (int i = 0; i < jsonArray.length(); i++) {
//                                    JSONObject object = jsonArray.getJSONObject(i);
//                                    statecodelist.add(object.getString("region_id"));
//                                    statelabellist.add(object.getString("name"));
//                                }
//                                populateData(R.string.State);
//                            } else {
////                                Utils.showToast(RegistrationStep2Activity.this, getString(R.string.msg_itSeems));
//                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                });
//                mAuthTask.execute();
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    /**
//     * This method is used to get City List
//     */
//    private void getCityList(String region_id) {
//        //call API
//
//        RequestBody mReqBody = new FormBody.Builder()
//                .add("region", region_id)
//                .build();
//        try {
//            if (Utils.isOnline(this)) {
//                mAuthTask = new HttpFormRequest(this, URLConstants.cityUrl, "",
//                        mReqBody, true, new HttpFormRequest.AsyncTaskCompleteListener() {
//                    @Override
//                    public void asyncTaskComplted(String response) {
//                        mAuthTask = null;
//                        try {
//                            if (response != null) {
//
//                                JSONObject jsonObject = new JSONObject(response);
////
//                                JSONObject jsonObjectData = jsonObject.getJSONObject("data");
//
//                                JSONArray jsonArray = jsonObjectData.getJSONArray("city");
//                                for (int i = 0; i < jsonArray.length(); i++) {
//                                    JSONObject object = jsonArray.getJSONObject(i);
//                                    citycodelist.add(object.getString("value"));
//                                    citylist.add(object.getString("label"));
//                                }
//                                populateData(R.string.City);
//                            } else {
////                                Utils.showToast(RegistrationStep2Activity.this, getString(R.string.msg_itSeems));
//                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                });
//                mAuthTask.execute();
//            }
//
//
////            ClientRequestResponse crr = new ClientRequestResponse(new AsyncResponse() {
////                @Override
////                public void processFinish(Object output) throws JSONException {
////
////                    JSONObject jsonObject = new JSONObject(output.toString());
////
////                    JSONObject jsonObjectData = jsonObject.getJSONObject("data");
////
////                    JSONArray jsonArray=jsonObjectData.getJSONArray("city");
////                    for(int i=0;i<jsonArray.length();i++)
////                    {
////                        JSONObject object=jsonArray.getJSONObject(i);
////                        citycodelist.add(object.getString("value"));
////                        citylist.add(object.getString("label"));
////                    }
////
////                    populateData(R.string.City);
////
////                }
////            }, RegisterStep2.this,"POST",hashMap);
////            crr.execute(URLConstant.CITY_URL);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    /** Method to bind value to Dropdown*/
//    public void populateData(int type) {
//
//        switch (type) {
//            case R.string.Country:
//                // Add Data to country Spinner
//                if (countrylabellist != null && countrylabellist.size() > 0) {
//
//                    PopupWindow popUp = popupWindowsort(countrylabellist, type);
//                    popUp.showAsDropDown(spCountry, 0, 0); //
//
//                } else {
//                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.alert_nodata), Toast.LENGTH_LONG).show();
//                }
//                break;
//            case R.string.State:
//                // Add Data to country Spinner
//                if (statelabellist != null && statelabellist.size() > 0) {
//
//                    PopupWindow popUp = popupWindowsort(statelabellist, type);
//                    popUp.showAsDropDown(spState, 0, 0); //
//
//                } else {
//                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.alert_nodata), Toast.LENGTH_LONG).show();
//                }
//                break;
//            case R.string.City:
//                // Add Data to country Spinner
//                if (citylist != null && citylist.size() > 0) {
//
//                    PopupWindow popUp = popupWindowsort(citylist, type);
//                    popUp.showAsDropDown(spCity, 0, 0); //
//
//                } else {
//                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.alert_nodata), Toast.LENGTH_LONG).show();
//                }
//                break;
//        }
//
//    }
//
//    private PopupWindow popupWindowsort(final ArrayList<String> contentArray, final int type) {
//
//        // initialize a pop up window type
//        popupWindow = new PopupWindow(this);
//
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(RegistrationStep2Activity.this, android.R.layout.simple_list_item_1,
//                contentArray);
//        // the drop down list is a list view
//        ListView listViewSort = new ListView(RegistrationStep2Activity.this);//(ListView)layout.findViewById(R.id.listView);
//        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        param.setMargins(16, 0, 16, 0);
//        listViewSort.setLayoutParams(param);
//        listViewSort.setSmoothScrollbarEnabled(true);
//        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
//            listViewSort.setScrollBarSize(5);
//        }
//        // set our adapter and pass our pop up window contents
//        listViewSort.setAdapter(adapter);
//
//        // set on item selected
//        listViewSort.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//                switch (type) {
//                    case R.string.Country:
//                        statecodelist.clear();
//                        statelabellist.clear();
//                        citycodelist.clear();
//                        citylist.clear();
//                        mSelState = "";
//                        spState.setText("");
//                        spCity.setText("");
//                        spCountry.setText(countrylabellist.get(position));
//                        mSelCountry = countrycodelist.get(position);
//                        break;
//                    case R.string.State:
//                        citycodelist.clear();
//                        citylist.clear();
//                        mSelCity = "";
//                        spCity.setText("");
//                        spState.setText(statelabellist.get(position));
//                        mSelState = statecodelist.get(position);
//                        break;
//                    case R.string.City:
//                        spCity.setText(citylist.get(position));
//                        mSelCity = citycodelist.get(position);
//                        break;
//                }
//
//                if (popupWindow != null) {
//                    popupWindow.dismiss();
//                }
//            }
//        });
//
//        // some other visual settings for popup window
//        popupWindow.setFocusable(true);
//        popupWindow.setWidth(mPopupWindowWidth);
//        popupWindow.setBackgroundDrawable(getResources().getDrawable(R.drawable.popup_background));
//        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
//
//        // set the listview as popup content
//        popupWindow.setContentView(listViewSort);
//
//        return popupWindow;
//    }
//    /*private void register()
//    {
//
//        try {
//            if (MainActivity.NoModule) {
//                circularProgressBar.setVisibility(View.GONE);
//                loaderimage.setVisibility(View.GONE);
//                jsonObj = new JSONObject(jstring);
//                if (jsonObj.has("header") && jsonObj.getString("header").equals("false")) {
//                    // Toast.makeText(getApplicationContext(), getResources().getString(R.string.url_not_found), Toast.LENGTH_LONG).show();
//                    Intent intent = new Intent(getApplicationContext(), UnAuthourizedRequestError.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(intent);
//                    overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
//                } else {
//                    response = jsonObj.getJSONObject(KEY_OBJECT).getJSONArray(KEY_CUSTOMER);
//                    for (int i = 0; i < response.length(); i++) {
//                        JSONObject c = null;
//                        c = response.getJSONObject(i);
//
//                        status = c.getString(KEY_STATUS);
//                        if (status.equals("error")) {
//                            message = c.getString(KEY_MESSAGE);
//                        }
//                        if (status.equals("success")) {
//                            isConfirmationRequired = c.getString(KEY_IS_CONFIRMATION_REQUIRED);
//                            message = c.getString(KEY_MESSAGE);
//                        }
//                    }
//                    if (status.equals("success")) {
//                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
//                        if (isConfirmationRequired.equals("NO")) {
//                            HashMap<String, String> hashMap = new HashMap<String, String>();
//                            hashMap.put("email", hashMap_stored.get("email"));
//                            hashMap.put("password", hashMap_stored.get("password"));
//                            if(management.getCartId()!=null)
//                            {
//                                hashMap.put("cart_id", management.getCartId());
//                            }
//                            try {
//                                ClientRequestResponse response = new ClientRequestResponse(new AsyncResponse() {
//                                    @Override
//                                    public void processFinish(Object output) {
//                                        outputstring = output.toString();
//                                        checklogin();
//                                    }
//                                }, RegisterStep2.this, "POST", hashMap);
//                                response.execute(LoginUrl);
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//
//
//                        } else {
//                            Intent popActivity = new Intent(getApplicationContext(), Login.class);
//                            popActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            startActivity(popActivity);
//                            overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
//
//                        }
//                    } else {
//                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
//                    }
//                }
//            }
//            else
//            {
//                Toast.makeText(getApplicationContext(), getResources().getString(R.string.url_not_found), Toast.LENGTH_LONG).show();
//                Intent intent=new Intent(getApplicationContext(), NoModule.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(intent);
//                overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//
//    }
//
//    private void checklogin() {
//        try
//        {
//            if(MainActivity.NoModule)
//
//            {
//                circularProgressBar.setVisibility(View.GONE);
//                loaderimage.setVisibility(View.GONE);
//                jsonObj = new JSONObject(outputstring);
//                if (jsonObj.has("header") && jsonObj.getString("header").equals("false")) {
//                    // Toast.makeText(getApplicationContext(), getResources().getString(R.string.url_not_found), Toast.LENGTH_LONG).show();
//                    Intent intent = new Intent(getApplicationContext(), UnAuthourizedRequestError.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(intent);
//                    overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
//                } else {
//
//                    response = jsonObj.getJSONObject(KEY_OBJECT).getJSONArray(KEY_CUSTOMER);
//                    for (int i = 0; i < response.length(); i++) {
//                        JSONObject c = null;
//                        c = response.getJSONObject(i);
//
//                        status = c.getString(KEY_STATUS);
//                        if (status.equals("success")) {
//                            customer_id = c.getString(KEY_CUSTOMER_ID);
//                            hash = c.getString(KEY_HASH);
//                            cart_summary = c.getString(KEY_CART_SUMMARY);
//                            MainActivity.latestcartcount = cart_summary;
//                        } else if (status.equals("exception")) {
//                            message = c.getString(KEY_Message);
//                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
//                        }
//
//                    }
//                    if (status.equals("success")) {
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.succesfullLogin), Toast.LENGTH_LONG).show();
//                        session.createLoginSession(hash, hashMap_stored.get("email"));
//
//                        session.saveCustomerId(customer_id);
//                        Intent homeintent = new Intent(RegisterStep2.this, Homepage.class);
//                        homeintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        homeintent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        startActivity(homeintent);
//                        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
//                    }
//
//                }
//            }
//            else
//            {
//                Toast.makeText(getApplicationContext(), getResources().getString(R.string.url_not_found), Toast.LENGTH_LONG).show();
//                Intent intent=new Intent(getApplicationContext(), NoModule.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(intent);
//                overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
//            }
//        } catch (JSONException e)
//        {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    protected void onResume()
//    {
//        ConnectionDetector connectionDetector=new ConnectionDetector(RegisterStep2.this);
//        if(connectionDetector.isConnectingToInternet())
//        {
//            invalidateOptionsMenu();
//            super.onResume();
//        }
//        else
//        {
//            Intent intent=new Intent(getApplicationContext(), NoInternetconnection.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(intent);
//            overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
//            super.onResume();
//        }
//    }*/
//
//    @Override
//    public void onBackPressed() {
//        invalidateOptionsMenu();
//        super.onBackPressed();
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                this.finish();
//                return true;
//
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//}
