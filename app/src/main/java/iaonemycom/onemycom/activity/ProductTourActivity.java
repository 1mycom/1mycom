package iaonemycom.onemycom.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bv.commonlibrary.util.Pref;
import com.bv.commonlibrary.util.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import iaonemycom.onemycom.R;
import iaonemycom.onemycom.adapter.ProdcutTourPagerAdapter;
import iaonemycom.onemycom.util.PrefKey;

public class ProductTourActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    @BindView(R.id.viewPager)
    ViewPager mViewPager;

    @BindView(R.id.viewPagerCountDots)
    LinearLayout viewPagerCountDots;


    ProdcutTourPagerAdapter mAdapter;
    private int dotsCount;
    private ImageView[] dots;

    ArrayList<Integer>mList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_tour);

        ButterKnife.bind(this);
        Pref.openPref(this);
        mList = new ArrayList<>();

        //---Do not show Product tour again
        Pref.setValue(ProductTourActivity.this, PrefKey.KEY_CAN_SHOW_WELCOME, false);

        setPager();
//        Utils.setStatusBarColor(this, getResources().getColor(R.color.statusbar_color));
    }



    private void setPager() {
        mList.add(R.drawable.tour_1);
        mList.add(R.drawable.tour_2);
        mList.add(R.drawable.tour_3);

        mAdapter = new ProdcutTourPagerAdapter(ProductTourActivity.this, mList);
        mViewPager.setAdapter(mAdapter);
        mViewPager.setCurrentItem(0);
        mViewPager.setOnPageChangeListener(this);
        setUiPageViewController();


    }

    @OnClick(R.id.txtSkip)
    public void skipClick(View view) {
        Utils.setRippleEffect(this, view);



        Intent intent = new Intent(ProductTourActivity.this, MainActivity.class);
//        intent.putExtra(AppConfig.KEY_ACTIVITY, "WelcomeActivity");
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    @OnClick(R.id.txtNext)
    public void nextClick(View view) {
        Utils.setRippleEffect(this, view);
        if(mViewPager.getCurrentItem() < 2) {
            int currentPosition = mViewPager.getCurrentItem() + 1;
            mViewPager.setCurrentItem(currentPosition);
            setPagerDots(currentPosition);
        }
        else {
            Intent intent = new Intent(ProductTourActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
        }

    }

    private void setUiPageViewController() {
        dotsCount = mAdapter.getCount();
        dots = new ImageView[dotsCount];
        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.normal_dot));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(4, 0, 4, 0);
            viewPagerCountDots.addView(dots[i], params);
        }
        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.selected_dot));
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        setPagerDots(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    private void setPagerDots(int position)
    {
        for (int i = 0; i < dotsCount; i++) {
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.normal_dot));
        }
        dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selected_dot));
    }
}
