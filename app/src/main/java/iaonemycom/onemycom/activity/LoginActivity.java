package iaonemycom.onemycom.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.bv.commonlibrary.util.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import iaonemycom.onemycom.R;


public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.coordinatorlayout)
    CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        Utils.setStatusBarColor(this, R.color.gray_dark);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if(requestCode==1001)
//        {
            finish();
//        }
    }
}