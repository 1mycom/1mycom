package iaonemycom.onemycom.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.bv.commonlibrary.util.Pref;
import com.bv.commonlibrary.util.Utils;

import iaonemycom.onemycom.R;
import iaonemycom.onemycom.util.PrefKey;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Pref.openPref(SplashActivity.this);

        startNextScreen();
    }

    private void startNextScreen() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Utils.isNotNull(Pref.getValue(SplashActivity.this, PrefKey.KEY_USERID, ""))) {
                    startActivity(new Intent(getApplicationContext(), MainActivity
                            .class));
                    SplashActivity.this.finish();
                    overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                } else {

                    if (Pref.getValue(SplashActivity.this, PrefKey.KEY_CAN_SHOW_WELCOME, true))
                        startActivity(new Intent(getApplicationContext(), ProductTourActivity.class));
                    else
                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    SplashActivity.this.finish();
                    overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                }
            }
        }, 2000);
    }
}
