package iaonemycom.onemycom.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.bv.commonlibrary.util.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import iaonemycom.onemycom.R;
import iaonemycom.onemycom.constants.Constants;
import iaonemycom.onemycom.fragment.RegistrationStep1Fragment;
import iaonemycom.onemycom.util.ActivityNavigationEnum;

/**
 */
public class RegistrationStepActivity extends AppCompatActivity {

    @BindView(R.id.frameLayout)
    FrameLayout mFrameLayout;
    private FragmentManager fm;

    RegistrationStep1Fragment mRegistrationStep1Fragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_step1);

        ButterKnife.bind(this);

        fm = getSupportFragmentManager();

        mRegistrationStep1Fragment = RegistrationStep1Fragment.newInstance(ActivityNavigationEnum.FROM_LOGIN.getValue());
        pushFragments(Constants.TAG_FRAGMENT_REG_STEP1, R.id.frameLayout,mRegistrationStep1Fragment, true);
//        fm.beginTransaction()
//                .setCustomAnimations(R.anim.pull_in_right,
//                        R.anim.push_out_left, R.anim.pull_in_left,
//                        R.anim.push_out_right)
//
//                .replace(R.id.frameLayout, RegistrationStep1Fragment.newInstance()).addToBackStack(null)
//                .commit();

        Utils.setStatusBarColor(this, R.color.blue_statusbar);
    }

    @Override
    public void onBackPressed() {
        popFragments();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    public void pushFragments(String tag, int container, Fragment fragment, boolean canAddToBackStack) {
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(R.anim.pull_in_right, R.anim.push_out_left, R.anim.pull_in_left, R.anim.push_out_right);
        ft.replace(container, fragment, tag);
        if (canAddToBackStack)
            ft.addToBackStack(tag);
        ft.commit();
    }

    public void addFragments(String tag, int container, Fragment fragment, boolean canAddToBackStack) {
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(R.anim.pull_in_right, R.anim.push_out_left, R.anim.pull_in_left, R.anim.push_out_right);
        ft.add(container, fragment, tag);
        ft.hide(mRegistrationStep1Fragment);
        if (canAddToBackStack)
            ft.addToBackStack(tag);
        ft.commit();
    }

    public void popFragments() {

        if (fm.getBackStackEntryCount() == 1) {
            finish();
            overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
        } else
            fm.popBackStack();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==1001)
        {
            setResult(1001);
            finish();
        }
    }
}
