package iaonemycom.onemycom.constants;

import iaonemycom.onemycom.BuildConfig;

/**
 * Created by jay on 8/3/17.
 */

public class URLConstants {

    public static String customerLogin= BuildConfig.BaseURL+"mobiconnect/customer_account/login";
    public static String socialUrl = BuildConfig.BaseURL+"mobiconnectsociallogin/customer/register";

    public static String countryUrl=BuildConfig.BaseURL+"mobiconnect/customer_account/countryCollection";
    public static String stateUrl=BuildConfig.BaseURL+"mobiconnect/customer_account/getCountryRegion";
    public static String cityUrl=BuildConfig.BaseURL+"mobiconnect/customer_account/getRegionCity";

    public static String registerUrl=BuildConfig.BaseURL+"mobiconnect/customer_account/register";

}
